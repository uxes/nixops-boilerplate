{
  network.description = "bertikov.cz infrastructure";

  prod =
    { config, lib, pkgs, ... }:
    {
      imports = [
        ./env.nix
        ./machines/prod.nix
	./ct.nix
      ];

      #12257 nixos-slave
      # deployment.targetHost = "prod";

      deployment.targetHost = "37.205.14.42";
#deployment.targetEnv = "dumb";
    };
}
