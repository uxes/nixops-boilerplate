{ config, lib, pkgs, ... }:
let
  appPort = 31337;
  wsPort = 8000;
in
{

  networking.firewall.allowedTCPPorts = [ 80 ];

  services.nginx = {
    enable = true;
    virtualHosts = {
      "default" = {
        default = true;
        locations."/" = {
          proxyPass = "http://uxes.cz";
        };
      };
    };
  };
}
