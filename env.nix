{ config, pkgs, ... }:
{
  time.timeZone = "Europe/Amsterdam";
  services.openssh.enable = true;

#  nix.useSandbox = false;

  environment.systemPackages = with pkgs; [
    wget
    vim
    screen
  ];

  users.extraUsers.root.openssh.authorizedKeys.keys =
    with import ./ssh-keys.nix; [ masox ];

}
